---
title: Cloud Patterns - Best Effort Master Election
description: Master elections are really hard, but very useful. Here's a simple pattern for a best effort master election.
date: 2020-05-27
tags:
  - cloud-pattern
  - distributed-systems
layout: layouts/post.njk
---
It would not be an overstatement to say that one of the hardest problems in distributed systems is master election. Entire complex algorithms, designed and vetted by many, many PhDs have been developed to solve this (Paxos, Raft, etc.).

